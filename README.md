# URL Shortener example microservice

## Developing

Requires requires global install of CompileDaemon - see ./dev.sh file comments.

`bash ./dev.sh`

## Building

`docker-compose build`

## Running local demo in Docker

`docker-compose up`

## Example usage

Creating a short URL:
`curl localhost:8080/api/shorten-url -d '{"originalURL": "http://example.com"}'`

Using a short URL:
`curl http://localhost:8080/eoFT99wPV9LzRicQ2X0Ayw -L`

Docker image usage:
- see docker-compose.yml
