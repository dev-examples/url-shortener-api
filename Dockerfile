FROM golang:1.14.2-alpine3.11
WORKDIR /builder
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o url-shortener-api

FROM alpine:3.11
WORKDIR /app
COPY --from=0 /builder/url-shortener-api .
ENTRYPOINT [ "/app/url-shortener-api" ]
ENV GIN_MODE=release
