package shortener

import (
	"crypto/md5"
	"encoding/base64"
	"fmt"

	"github.com/gin-gonic/gin"
)

func hashString(data string) string {
	hashBytes := md5.Sum([]byte(data))
	hashString := base64.RawStdEncoding.EncodeToString(hashBytes[:])
	return hashString
}

func generateKey(data string, prefix string) string {
	hash := hashString(data)
	key := fmt.Sprintf("%v/%v", prefix, hash)
	return key
}

func composeShortURL(key string, c *gin.Context) string {
	scheme := "http"
	if c.Request.TLS != nil {
		scheme = "https"
	}
	host := c.Request.Host
	shortURL := fmt.Sprintf("%v://%v%v", scheme, host, key)
	return shortURL
}
