package shortener

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.korz.ec/url-shortener-api/middleware/storage"
)

// HandleShortURLForward is a request handler
// for short URL that forwards to original URL
// Requires dependencies to be provided by middleware:
// - "STORE" of type storage.Storage
func HandleShortURLForward(c *gin.Context) {
	store := c.MustGet("STORE").(storage.Storage)
	path := c.Request.URL.Path

	// shortBaseURL := c.MustGet("FORWARDER_PREFIX").(string)
	// if isMatching, _ := regexp.MatchString("^"+shortBaseURL, path); !isMatching {
	// 	// TODO: anything to do if value is not matching the shortBaseURL ?
	// }

	if originalURL, err := store.Read(path); err != nil {
		c.String(http.StatusOK, "Error: "+err.Error())
	} else {
		c.Redirect(http.StatusSeeOther, originalURL)
	}
}
