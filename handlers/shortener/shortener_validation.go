package shortener

import (
	"reflect"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func validateNoWhitespace(fl validator.FieldLevel) bool {
	if ok, err := regexp.MatchString(`\s`, fl.Field().String()); ok || err != nil {
		return false
	}
	return true
}

// make validator use json field names
func getTagNameFromJSON(fld reflect.StructField) string {
	name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
	if name == "-" {
		return ""
	}
	return name
}

// RegisterValidation registers custom validations
// in Gin's global validator engine;
// should be run before any requests are processed
func RegisterValidation() {
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("nowhitespace", validateNoWhitespace)
		v.RegisterTagNameFunc(getTagNameFromJSON)
	}
}

type object map[string]interface{}

type fieldValidationResult struct {
	Name    string `json:"name"`
	Message string `json:"message"`
}

func formatValidationErrors(errors *validator.ValidationErrors) interface{} {
	var messages []string
	var results []fieldValidationResult

	for _, fieldErr := range *errors {

		var message string
		switch tag := fieldErr.ActualTag(); tag {
		case "required":
			message = "is required"
		case "url":
			message = "should be a valid URL"
		case "nowhitespace":
			message = "should not contain whitespace"
		case "max":
			message = "should not exceed " + fieldErr.Param() + " characters"
		default:
			message = "should satisfy rule: " + tag
		}

		fieldResult := fieldValidationResult{fieldErr.Field(), message}
		results = append(results, fieldResult)

		// messages = append(messages, error(errors).Error()) // default error strings
		messages = append(messages, fieldErr.Field()+" "+message) // custom error strings
	}

	return object{
		"error":  "Validation error: " + strings.Join(messages, ", "),
		"fields": results,
	}
}
