package shortener

import (
	"testing"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
)

// references
// https://github.com/GoogleCloudPlatform/cloudspanner-ticketshop-demo/blob/master/backend/vendor/gopkg.in/go-playground/validator.v9/validator_test.go
// https://github.com/go-playground/validator/issues/342

var whitespaceTestStrings = []struct {
	have string
	want bool
}{
	{"", true},
	{"xxx", true},
	{" ", false},
	{" xxx", false},
	{"xxx ", false},
}

func TestValidateNoWhitespace(t *testing.T) {
	validate := validator.New()
	validate.RegisterValidation("nowhitespace", validateNoWhitespace)

	for _, item := range whitespaceTestStrings {
		err := validate.Var(item.have, "nowhitespace")
		if item.want {
			assert.Nil(t, err)
		} else {
			assert.Error(t, err)
		}
	}
}

func TestFormatValidationErrors(t *testing.T) {
	validate, _ := binding.Validator.Engine().(*validator.Validate)
	validate.RegisterValidation("nowhitespace", validateNoWhitespace)
	validate.RegisterTagNameFunc(getTagNameFromJSON)

	errors := validate.Struct(URLShortingRequest{
		OriginalURL: "x",
	})
	validationErrors := errors.(validator.ValidationErrors)

	assert.Equal(t, 1, len([]validator.FieldError(validationErrors)))

	formatted := formatValidationErrors(&validationErrors).(object)

	want := "Validation error: originalURL should be a valid URL"
	assert.Equal(t, want, formatted["error"])
}
