package shortener

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.korz.ec/url-shortener-api/middleware/storage"
)

// URLShortingRequest is a model for URL shortening request data
type URLShortingRequest struct {
	OriginalURL string `json:"originalURL" binding:"required,url,nowhitespace,max=8192"`
}

// URLShortingResponse is a model for URL shortening Response data
type URLShortingResponse struct {
	OriginalURL string `json:"originalURL"`
	ShortURL    string `json:"shortURL"`
	Warning     string `json:"warning,omitempty"`
}

// HandleURLShorting is a request handler
// that validates URL shortening requests
// and sends a short URL back if successful
// Requires dependencies to be provided by middleware:
// - "STORE" of type storage.Storage
// - "FORWARDER_PREFIX" of type string
func HandleURLShorting(c *gin.Context) {
	var data URLShortingRequest
	// parse and validate body
	if err := c.ShouldBindJSON(&data); err != nil {
		validationErrors, ok := err.(validator.ValidationErrors)
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
		result := formatValidationErrors(&validationErrors)
		c.JSON(http.StatusBadRequest, result)
		return
	}
	// make short URL and store with original URL as key-value
	prefix := c.GetString("FORWARDER_PREFIX")
	key := generateKey(data.OriginalURL, prefix)
	storage := c.MustGet("STORE").(storage.Storage)

	if err := storage.Save(key, data.OriginalURL); err != nil {
		// if key is already in store check if original URL matches
		originalURL, readErr := storage.Read(key)
		if readErr == nil && originalURL == data.OriginalURL {
			c.JSON(http.StatusOK, URLShortingResponse{
				OriginalURL: data.OriginalURL,
				ShortURL:    composeShortURL(key, c),
				Warning:     "The URL was already created before",
			})
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, URLShortingResponse{
		OriginalURL: data.OriginalURL,
		ShortURL:    composeShortURL(key, c),
	})
}
