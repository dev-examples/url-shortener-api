package main

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	"go.korz.ec/url-shortener-api/handlers/shortener"
	"go.korz.ec/url-shortener-api/middleware/storage"
)

func main() {
	// get from env: PORT, FORWARDER_PREFIX, STORE_FILE
	forwarderPrefix := os.Getenv("FORWARDER_PREFIX")
	storeFile := os.Getenv("STORE_FILE")
	if storeFile == "" {
		storeFile = "./data/store.json"
		fmt.Println("STORE_FILE not defined, using default: " + storeFile)
	}

	store, _ := storage.CreateFileStorage(storeFile)

	// add middleware to pass the store and shortBaseURL to handlers
	setVariables := func(c *gin.Context) {
		c.Set("FORWARDER_PREFIX", forwarderPrefix)
		c.Next()
	}

	router := gin.Default()
	// make storage available in handlers
	router.Use(storage.ConnectStore(store))
	// make variables such as shortBaseURL available in handlers
	router.Use(setVariables)

	shortener.RegisterValidation()

	router.POST("/api/shorten-url", shortener.HandleURLShorting)
	router.GET(forwarderPrefix+"/*short", shortener.HandleShortURLForward)

	fmt.Printf("Running API server in '%v' mode\n", gin.Mode())

	router.Run()
}
