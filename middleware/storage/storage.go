package storage

import "github.com/gin-gonic/gin"

// Storage is a key value storage interface where keys and values are strings
type Storage interface {
	Save(string, string) error
	Read(string) (string, error)
}

// ConnectStore is a minimal middleware handler
// that passes a Storage object to request handlers
func ConnectStore(store Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("STORE", store)
		c.Next()
	}
}
