package storage

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type testVariables struct {
	fileName string
}

func setup() testVariables {
	v := testVariables{
		fileName: "test-file" + uuid.New().String() + ".json",
	}
	os.Remove(v.fileName)
	return v
}

func teardown(v testVariables) {
	os.Remove(v.fileName)
}

func TestFileStorage(t *testing.T) {
	v := setup()
	defer teardown(v)
	store, err := CreateFileStorage(v.fileName)

	assert.NotNil(t, store)
	assert.Nil(t, err)

	bytes, _ := ioutil.ReadFile(v.fileName)
	want := "{\n  \"links\": {}\n}"
	assert.Equal(t, want, string(bytes))
}

func TestFileRead(t *testing.T) {
	v := setup()
	defer teardown(v)

	ioutil.WriteFile(v.fileName, []byte(`{"links":{"test read":"test save value"}}`), 0644)

	store, _ := CreateFileStorage(v.fileName)
	key := "test read"

	value, err := store.Read(key)
	assert.Nil(t, err)
	want := "test save value"
	assert.Equal(t, want, value)
}

func TestFileSave(t *testing.T) {
	v := setup()
	defer teardown(v)
	store, _ := CreateFileStorage(v.fileName)
	key := "test save"
	value := "test save value"

	err := store.Save(key, value)
	assert.Nil(t, err)

	bytes, _ := ioutil.ReadFile(v.fileName)
	want := "{\n  \"links\": {\n    \"test save\": \"test save value\"\n  }\n}"
	assert.Equal(t, want, string(bytes))
}

func TestConcurrentWrites(t *testing.T) {

}
