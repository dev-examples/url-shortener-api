package storage

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)

// CreateFileStorage creates FileStorage with a given file name
// and creates a JSON storage file if does not exist
func CreateFileStorage(fileName string) (*FileStorage, error) {
	store := &FileStorage{
		fileName: fileName,
		mutex:    sync.RWMutex{},
	}

	if _, err := ioutil.ReadFile(fileName); err != nil {
		fmt.Println(err.Error())
		fmt.Printf("Creating new %s\n", fileName)
		os.MkdirAll(filepath.Dir(fileName), 0644)
		err := writeJSON(fileName, &fileStorageModel{
			Links: make(map[string]string), // make sure the Links map will be available
		})
		if err != nil {
			return nil, errors.New("Error creating JSON: " + err.Error())
		}
	}
	return store, nil
}

// FileStorage stores key-value pairs in JSON file and can receive values
type FileStorage struct {
	fileName string
	mutex    sync.RWMutex
}

type fileStorageModel struct {
	Links map[string]string `json:"links"`
}

// Read retrieves a value for a given key
func (s *FileStorage) Read(key string) (string, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	data, err := readJSON(s.fileName)
	if err != nil {
		return "", fmt.Errorf("Store file error: %v", err)
		//errors.New("Store file error: " + err.Error())
	}
	value, ok := data.Links[key]
	if !ok {
		return "", fmt.Errorf("No value for key '%s'", key)
	}
	return value, nil
}

// Save stores a key-value pair
func (s *FileStorage) Save(key string, value string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	data, err := readJSON(s.fileName)
	if err != nil {
		return errors.New("File error: " + err.Error())
	}

	if _, exists := data.Links[key]; exists {
		return errors.New("Key already exists")
	}

	data.Links[key] = value
	err = writeJSON(s.fileName, data)
	if err != nil {
		return errors.New("Saving error: " + err.Error())
	}
	return nil
}
