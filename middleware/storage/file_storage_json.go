package storage

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

func readJSON(fileName string) (*fileStorageModel, error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, errors.New("Cannot read: " + err.Error())
	}

	var data fileStorageModel

	err = json.Unmarshal(file, &data)
	if err != nil {
		return nil, errors.New("Cannot parse: " + err.Error())
	}
	return &data, nil
}

func writeJSON(fileName string, data *fileStorageModel) error {
	file, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return errors.New("Cannot serialise: " + err.Error())
	}
	err = ioutil.WriteFile(fileName, file, 0644)
	if err != nil {
		return errors.New("Cannot write: " + err.Error())
	}
	return nil
}
